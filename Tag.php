<?php
/**
 * Created by PhpStorm.
 * User: IVAN
 * Date: 22.06.2018
 * Time: 11:13
 */

namespace EasyCode\domCreator;


class Tag
{
    public $tag = '';
    public $attributes = [];
    public $tree = [];

    public function __construct(string $tag, array $attributes, array $tree = [])
    {
        $this->tag = $tag;
        $this->attributes = $attributes;
        $this->tree = $tree;
    }

    public  function __toString()
    {
        return implode(' ', $this->tree);
    }

    public function text()
    {
        return $this->__toString();
    }

    public function attr( string $attr) : string
    {
        return $this->attributes[$attr];
    }

    public function find(array $filter) : Tag
    {
        if ( !self::validateFilter($filter) ) throw new \Error('неверные параметры поиска по дереву');
        $findList =[];
        $this->walk($filter, $findList);
        if ( count($findList) == 1) {
            return array_shift($findList);
        }
        return new Tree('', [], $findList);
    }

    private function walk($filter, &$findList)
    {
        foreach ( $this->tree as $unit ){
            if ( !($unit instanceof Tag) ) continue;
            $isFind = true;
            if ($filter['tag'] && $filter['tag'] !== $unit->tag) $isFind = false;
            if ($filter['attr']){
                foreach ($filter['attr'] as $attr => $regExp){
                    if ( !isset($unit->attributes[$attr]) ||
                        !preg_match("/" . addslashes($regExp) . "/si", $unit->attributes[$attr]))
                    {
                        $isFind = false;
                        break;
                    }
                }
            }
            if ( $isFind && !in_array($unit, $findList, true)){
                $findList[] = $unit;
            }
            $unit->walk($filter, $findList);
        }
    }

    private static function validateFilter(&$filter) : bool
    {
        $settings = [
            'tag' => 'string',
            'attr' => 'array',
        ];
        $params = array_keys($settings);
        foreach ($filter as $key => $val){
            if (!in_array($key, $params, true))
                return false;
            if ($settings[$key] != gettype( $val ))
                return false;
        }
        foreach ($params as $key) {
            $filter[$key] = isset($filter[$key]) ? $filter[$key] : null;
        }
        return true;

    }

}