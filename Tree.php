<?php
/**
 * Created by PhpStorm.
 * User: IVAN
 * Date: 04.07.2018
 * Time: 16:39
 */

namespace EasyCode\domCreator;


class Tree extends Tag  implements \Iterator, \Countable, \ArrayAccess
{
    public function count()
    {
        return count($this->tree);
    }

    public function offsetExists($offset)
    {
        return isset($this->tree[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->tree[$offset];
    }

    public function offsetSet($offset, $value)
    {
    }

    public function offsetUnset($offset)
    {
    }

    public function rewind()
    {
        reset($this->tree);
    }

    public function current()
    {
        return current($this->tree);
    }

    public function key()
    {
        return key($this->tree);
    }

    public function next()
    {
        return next($this->tree);
    }

    public function valid()
    {
        $unitKey = key($this->tree);
        $unit = ($unitKey !== NULL && $unitKey !== FALSE);
        return $unit;
    }



}