<?php
/**
 * Created by PhpStorm.
 * User: IVAN
 * Date: 22.06.2018
 * Time: 11:11
 */

namespace EasyCode\domCreator;


abstract class GenerateDom
{
    public static function newDom(string &$content)
    {
        /*
         * тут идет костыль.
         * В будущем надо будет сделать нормализацию входного документа, расставить недостающие теги, потому, что какому
         * то ЛЕНИВОМУ МУДАКУ было лень закрыть тег. Руки бы поотрывать.
         * а пока дописывать в костыль
         */
        $content = str_replace(["\r","\n"],"", $content);
        $content = trim($content);
        $content = preg_replace("/\\s+(?=>)/","", $content);
        $content = preg_replace("/>\\s+</","><", $content);
        $content = preg_replace("/\\s{2,}/"," ", $content);
        $content = preg_replace("#(?<!</head>)<body#","</head><body", $content);
        $content = preg_replace("#(?<!</html>)$#","</html>", $content);
        $content = preg_replace("#(?<!</body>)</html>#","</body></html>", $content);
        $res = self::create($content);
        return $res['obj'];
    }

    private static function create(&$domStr)
    {
        $tag = null;
        $attr = [];
        $tree = [];
        $upstairs = null;

        preg_match(
            '~(.*?)
            (?:<(\\w+)((?:\\s[\\w,\-]+(?:\\s?=\\s?("|\')?.*?\\4?)?)*)\\s*/?>|</(\\w+)>)
            (.*)~ix', $domStr, $matches);

        $domStr = $matches[6];
        unset($matches[6]);
        unset($matches[4]);
        unset($matches[0]);

        if ( $matches[2]) {
            $upstairs = $upstairs ?? $matches[1];
            $tag = $tag ?? $matches[2];
            unset($matches[2]);
            $attr = $matches[3] ? self::splitAttributes($matches[3]) : [];
            unset($matches[3]);

            while ($domStr) {
                $res = self::create($domStr);
                if (isset($res['obj']) && $res['obj'] instanceof Tag) {
                    if ( isset($res['upstairs']) && $res['upstairs']) $tree[] = $res['upstairs'];
                    $tree[] = $res['obj'];
                } else {
                    if ($res['tag'] == $tag) {
                        if ( isset($res['upstairs']) && $res['upstairs']) $tree[] = $res['upstairs'];
                        $tree = array_merge($tree, $res['tree']);
                        return [
                            'upstairs' => $upstairs,
                            'obj' => new Tag($tag, $attr, $tree),
                        ];
                    } else {
                        if ( !$tag ) throw new DomValidateError('ошибка разбора дерква.');
                        $unit = new Tag($tag, $attr);
                        array_unshift($tree, $unit);
                        if ( isset($res['upstairs']) && $res['upstairs']) $tree[] = $res['upstairs'];
                        $tree = array_merge($tree, $res['tree']);
                        return [
                            'upstairs' => $upstairs,
                            'tag' => $res['tag'],
                            'tree' => $tree,
                        ];
                    }
                }
            }
        }
        if ($matches[5]) {
            if ($matches[1]) $tree[] = $matches[1];
            return [
                'tag' => $matches[5],
                'tree' => $tree,
            ];
        }
    }

    private static function splitAttributes(string $attrString) : array
    {
        preg_match_all('/([\\w,\-]+)(?:\\s?=\\s?(?:(?:("|\')(.*?)\\2)|(\\d+)))?/', $attrString, $matches, PREG_SET_ORDER);
        $res = [];
        foreach ( $matches as $attr) {
            $res[$attr[1]] = $attr[4] ?? $attr[3] ?? true;
        }
        return $res;
    }

}